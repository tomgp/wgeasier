WGEasier
========

These Vagrantfiles make easier using https://github.com/WeeJeWel/wg-easy in Windows by wrapping it inside a Vagrant box machine

There are two mode of operations:

* NAT: the guest machine is totally isolated from your network but anybody connecting to the VPN will be able to be under your network.

* Bridge: the guest machine is like another "device" inside your network with the ability of connecting to any other device inside it.

## Usage

1. Install VirtualBox
```
winget install Oracle.VirtualBox
```

2. Install Vagrant
```
winget install Hashicorp.Vagrant
```

3. Choose a mode of operation, get inside the chosen directory and run:
```
vagrant up
```

Note: if Bridge mode is used, Vagrant will ask you which network interface to bridge. Choose the one you use to connect to your network in the host machine.

Once the vagrant box is running, the wgconfs directory will be created and it will keep the WireGuard configurations

## Performance Tuning

You may have to adjust the part inside this block in the Vagrantfile:
```
   config.vm.provider "virtualbox" do |vb|
	...
	...
   end
```

Change NIC type, remove nested hardware virtualization or add more CPUs.

Be sure that you're using balanced or maximum performance in the Energy settings.